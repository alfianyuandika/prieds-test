const { user } = require("../models");

class UserController {
  async create(req, res) {
    try {
      let data = await user.create(req.body);
      return res.status(201).json({
        message: "Success",
        data,
      });
      console.log(data);
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}
module.exports = new UserController();
