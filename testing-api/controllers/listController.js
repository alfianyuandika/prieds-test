const { list } = require("../models");

class ListController {
  async create(req, res) {
    try {
      let data = await list.create(req.body);
      return res.status(201).json({
        message: "Success",
        data,
      });
      console.log(data);
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getAll(req, res) {
    try {
      let Errors = [];
      const options = {
        page: req.query.page,
        limit: req.query.limit,
				sort: { createdAt: -1 },
      };
      let data = await list.paginate({}, options);
      if (data.page > data.totalPages) {
        Errors.push("page not found");
      }
      if (Errors.length > 0) {
        return res.status(404).json({
          message: Errors.join(", "),
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal server error!",
        error: e,
      });
    }
  }
}
module.exports = new ListController();
