const mongoose = require("mongoose"); //import mongoose
const mongooseDelete = require("mongoose-delete"); // package to enable soft delete

const UserSchema = new mongoose.Schema(
  {
    nomorAntrian: {
      type: Number,
    },
    salesmanID: {
      type: Number,
      require: true,
    },
    customerName: {
      type: String,
      required: true,
    },
    pic: {
      type: String,
      require: true,
    },
    city: {
      type: String,
      require: true,
    },
    remark: {
      type: String,
      require: true,
    },
    npwp: {
      type: Number,
      require: true,
    },
    customerPriceCategory: {
      type: Number,
      require: true,
    },
    address: {
      type: String,
      required: true,
    },
    address2: {
      type: String,
      require: true,
    },
    contactNumber: {
      type: String,
      require: true,
    },
    region: {
      type: String,
      require: true,
    },
    province: {
      type: String,
      require: true,
    },
    kuota: {
      type: Number,
      require: true,
    },
    preferedExpedition: {
      type: String,
      require: true,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

UserSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("User", UserSchema, "User");
