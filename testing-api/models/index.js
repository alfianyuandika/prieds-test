const mongoose = require("mongoose"); //impo(t mongoose

const uri = process.env.MONGO_URI; //add URI mongo atlas

//connect to mongoDB
mongoose
  .connect(uri, {
    useUnifiedTopology: true, //must be added
    useNewUrlParser: true, //must be added
    // useCreateIndex: true, //must be added
    // useFindAndModify: false, //must be added
  })
  .then(() => console.log("MongoDB Connected!"))
  .catch((err) => console.error(err));

const user = require("./user");
const list = require("./list");


module.exports = { user, list };
