const mongoose = require("mongoose"); //import mongoose
const mongooseDelete = require("mongoose-delete"); // package to enable soft delete
const mongoosePaginate = require('mongoose-paginate-v2')

const ListSchema = new mongoose.Schema(
  {
    site: {
      type: String,
      require: true,
    },
    orderProductID: {
      type: String,
    },
    name: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      require: true,
    },
    price: {
      type: Number,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

ListSchema.plugin(mongoosePaginate);
ListSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("List", ListSchema, "List");
