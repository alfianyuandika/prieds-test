var express = require("express");
var router = express.Router();

const listController = require("../controllers/listController");

router.post("/", listController.create);
router.get("/", listController.getAll);

module.exports = router;
